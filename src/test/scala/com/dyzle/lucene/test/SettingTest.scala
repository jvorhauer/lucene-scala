package com.dyzle.lucene.test

import org.specs2.mutable.Specification
import org.specs2.runner.JUnitRunner
import org.junit.runner.RunWith
import com.dyzle.lucene.Setting._
import com.dyzle.lucene.Setting
import org.slf4j.LoggerFactory
import com.dyzle.lucene.test.SettingTest

/**
 * @author jvorhauer
 * @since 23/9/2011 13:27
 */
@RunWith(classOf[JUnitRunner])
class SettingTest extends Specification {

	val logger = LoggerFactory.getLogger(classOf[SettingTest])

	"A Setting" should {
		"Persist a new Setting" in {
			Setting.init()
			val firstSize = Setting.size
			logger.info("Persist: firstSize: {}", firstSize)
			val setting = Setting("TEST", "first.test.key", "Dyzle: your cold chain, our care")
			transaction {
				setting.save
			}
			logger.info("Persist: firstSize: {}, Setting.size: {}", firstSize, Setting.size)
			
			transaction {
				setting.delete()
			}

			Setting.size should beGreaterThanOrEqualTo (0)
		}

		"All Settings" in {
			Setting.init()
			val settings = Setting.all
			logger.info("All: settings.size: {}, Setting.size: {}", settings.size, Setting.size)
			settings.size should beGreaterThanOrEqualTo (0)
		}
		
		"Persist and find a Setting" in {
			Setting.init()
			val setting = Setting("TEST", "second.test.key", "Dyzle: your cold chain caretaker")
			transaction {
				setting.save
				Setting.commit()

				val found = Setting.find("TEST", "second.test.key")
				found should beSome

				val notFound = Setting.find("tost", "second.test.key")
				notFound should beNone

				setting.delete()
			}
			Setting.size should beGreaterThanOrEqualTo (0)
		}
	}
}
