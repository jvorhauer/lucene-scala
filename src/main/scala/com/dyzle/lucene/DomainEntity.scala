package com.dyzle.lucene

import org.apache.lucene.index._
import org.slf4j.LoggerFactory
import org.apache.lucene.document.{Field, Document}
import org.apache.lucene.search.IndexSearcher


/**
 * @author jvorhauer
 * @since 22/9/2011 13:36 PM
 */
abstract class DomainEntity {

  protected val logger = LoggerFactory.getLogger(this.getClass.getName)
	val writer: IndexWriter

	def id: String

	def save = {
    if (validate) {
	    val doc = makeDoc
	    logger.debug("save: {}", doc)
	    writer.addDocument(doc)
    }
		postSave
		this
	}

  def validate = true     // override to implement custom validation
  def postSave = true     // override to implement actions that should execute after save

	def delete() {
		val term = new Term("id", id)
		logger.debug("delete: id: {}", id)
		writer.deleteDocuments(term)
	}
	
	/**
	 * Override and extend to include the actual entity's relevant fields
	 * @return Document: a Lucene index document
	 */
	protected def makeDoc: Document = {
		val doc = new Document
		doc.add(new Field("id", id, Field.Store.YES, Field.Index.ANALYZED_NO_NORMS))
		doc
	}
	
	protected def makeField(name: String, value: String) = {
		new Field(name, value, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.WITH_POSITIONS_OFFSETS)
	}

	override def toString = this.getClass.getSimpleName + "(" + hashCode + ")"
}
