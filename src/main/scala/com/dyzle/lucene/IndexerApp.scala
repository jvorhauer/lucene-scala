package com.dyzle.lucene

import org.apache.lucene.util.Version
import org.apache.lucene.analysis.standard.StandardAnalyzer
import java.io.File
import org.apache.lucene.store.NIOFSDirectory
import scala.collection.JavaConverters._
import org.apache.lucene.document.{Field, Document}
import org.apache.lucene.index._
import java.util.{Date, Properties}
import org.apache.lucene.search._


/**
 * @author jvorhauer
 * @since 22/9/2011 11:58 AM
 */
object IndexerApp {

	val analyzer = new StandardAnalyzer(Version.LUCENE_34)
	val now = new Date
	val filedir = new File("/tmp/lucene/" + now.getTime)
	val directory = new NIOFSDirectory(filedir)

	def main(args: Array[String]) {
		createIndex()
		query("server*")
		query("spot*")
	}

	def createIndex() {
		val props = new Properties
		val stream = new PropertiesStream
		props.load(stream.load)

		val map = props.asScala

		if (!filedir.exists()) {
			filedir.mkdir()
		}
		println("createIndex: directory: " + filedir.getAbsolutePath)

		val writer = new IndexWriter(directory, new IndexWriterConfig(Version.LUCENE_34, analyzer))
		map.foreach(m => writer.addDocument(makeDoc(m._1, m._2)))
		writer.commit()
		writer.optimize()
		writer.close()

		println("createIndex: " + map.size + " properties found")
	}

	def makeDoc(key: String, value: String) = {
		val doc = new Document
		doc.add(new Field("key", key, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES))
		doc.add(new Field("value", value, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES))
		doc
	}

	def query(term: String) = {
		val searcher = new IndexSearcher(directory, true)
		val query = new WildcardQuery(new Term("key", term))
		val docs = searcher.search(query, 10)

		println("query: found " + docs.totalHits + " result(s)")

		docs.scoreDocs.foreach(id => {
			val d = searcher.doc(id.doc)
			println("\t" + d.get("key") + " = " + d.get("value"))
		})
	}
}

class PropertiesStream {

	def load = {
		this.getClass.getResourceAsStream("/spot.properties")
	}
}
