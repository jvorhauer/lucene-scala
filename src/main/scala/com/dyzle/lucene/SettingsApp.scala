package com.dyzle.lucene

import java.util.Properties
import scala.collection.JavaConverters._
import org.slf4j.LoggerFactory


/**
 * @author jvorhauer
 * @since 22/9/2011 15:03 PM
 */
object SettingsApp {

	val logger = LoggerFactory.getLogger("SettingsApp")

	def main(args: Array[String]) {
		
		Setting.init()

		logger.info("main: indexed {} properties", fillIndex)
		logger.info("main: Setting.size: {}", Setting.size)
		
		logger.info("main: found {} Settings with key:spot*", queryAllSpotKeys)
		logger.info("main: found {} Settings in total", queryAll)
		logger.info("main: found {} Setting for test & spot.index.url", queryTestSpotIndexUrl)
    logger.info("main: found {} Settings with QueryParser", queryText)

		Setting.close()
	}

	def fillIndex = {
		val props = new Properties
		val stream = new PropertiesStream
		props.load(stream.load)
		val map = props.asScala
    
		Setting.transaction {

			logger.debug("fillIndex: deleted {} settings", Setting.clear)

			map.foreach(m => {
				val setting = Setting("TEST", m._1, m._2)
				setting.save
			})
		}
		Setting.optimize()
		logger.debug("fillIndex: optimized")

    map.size
	}

	def queryAllSpotKeys = {
    logger.info("queryAllSpotKeys")
		val list = Setting.query("key", "spot*")
		list.foreach(s => logger.info(s.toString()))
		list.size
	}

	def queryAll = {
    logger.info("queryAll")
		val list = Setting.all
		list.foreach(s => logger.info(s.toString()))
		list.size
	}

	def queryTestSpotIndexUrl = {
		logger.info("queryTestSpotIndexUrl")
		val list = Setting.query(("env", "TEST"), ("key", "\"spot.index.url\""))
		list.foreach(s => logger.info(s.toString()))
		list.size
	}
  
  def queryText = {
	  logger.info("queryText")
    val list = Setting.query("env:TEST key:\"spot.index.url\"")
    list.foreach(s => logger.info(s.toString()))
  	list.size
  }
}
