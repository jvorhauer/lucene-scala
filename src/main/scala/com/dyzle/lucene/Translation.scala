package com.dyzle.lucene

import org.apache.lucene.document.Field

/**
 * @author jvorhauer
 * @since 23/9/2011 13:05
 */
case class Translation(language: String, key: String, var value: String) extends DomainEntity {
	val writer = Translation.writer

	override def id = hashCode.toString
	
	override def makeDoc = {
		val doc = super.makeDoc
		doc.add(new Field("language", language, Field.Store.YES, Field.Index.ANALYZED))
		doc.add(new Field("key", key, Field.Store.YES, Field.Index.ANALYZED))
		doc.add(new Field("value", value, Field.Store.YES, Field.Index.ANALYZED))
		doc
	}
	
	override def validate = {
		(language != null) && (language.length() == 2) && ("nl|en|fr|de|it|es".contains(language)) &&
		(key != null) && (key.length() > 1) &&
		!Translation.exists(id)
	}
}

object Translation extends Accessor[Translation] {
	
}
