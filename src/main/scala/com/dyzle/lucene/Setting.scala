package com.dyzle.lucene

import org.apache.lucene.document.Field
import org.apache.lucene.search.ScoreDoc
import com.dyzle.lucene.DomainEntity


/**
 * @author jvorhauer
 * @since 22/9/2011 13:43 PM
 */
case class Setting(env: String, key: String, var value: String) extends DomainEntity {

	override val writer = Setting.writer

	override def id: String = hashCode.toString
	
	override protected def makeDoc = {
		val doc = super.makeDoc
		doc.add(new Field("env", env, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES))
		doc.add(new Field("key", key, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES))
		doc.add(new Field("value", value, Field.Store.YES, Field.Index.ANALYZED, Field.TermVector.YES))
		doc
	}
  
  override def validate = {
    (env != null) && ("LOCAL|DEV|TEST|ACC|PROD".contains(env)) &&
    (key != null) && !Setting.exists(id)
  }

	override def toString = super.toString + ": " + key + " = " + value + " @ " + env
}


object Setting extends Accessor[Setting] {
  
  def mapper = (d: ScoreDoc) => {
      val document = searcher.doc(d.doc)
      Setting(document.get("env"), document.get("key"), document.get("value"))
    }

	def find(env: String, key: String) = {
		val results = query(("env", env), ("key", key))
		if (results.size == 0) {
			None
		} else {
			Option(results.head)      // actually max one...
		}
	}

	def find(key: String): List[Setting] = query("key", key)

	def all: List[Setting] = all(mapper)

	def query(field: String, value: String): List[Setting] = query((field, value))

	def query(params: Pair[String, String]*): List[Setting] = super.query(mapper, params: _*)
  
  def query(txt: String): List[Setting] = super.query(mapper, txt)
}
