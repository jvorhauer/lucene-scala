package com.dyzle.lucene

import org.apache.lucene.util.Version
import org.apache.lucene.analysis.standard.StandardAnalyzer
import java.io.File
import org.apache.lucene.store.NIOFSDirectory
import org.slf4j.LoggerFactory
import org.apache.lucene.index._
import org.apache.lucene.queryParser.QueryParser
import org.apache.lucene.search._

/**
 * @author jvorhauer
 * @since 22/9/2011 13:57 PM
 */
abstract class Accessor[T <: DomainEntity](implicit m: Manifest[T]) {

	val logger = LoggerFactory.getLogger(m.erasure.getName)

	val filedir = fileDir(m.erasure.getSimpleName)
	val directory = new NIOFSDirectory(filedir)
	lazy val searcher = new IndexSearcher(directory, true)
	val analyzer = new StandardAnalyzer(Version.LUCENE_34)
	val writer = new IndexWriter(directory, new IndexWriterConfig(Version.LUCENE_34, analyzer))

	/**
	 * Construct a query, execute it and return the results as List of T, the Entity Accessor's type
	 * @param f: (ScoreDoc) => T the function to create the correct return type
	 * @param params one or more pairs for field/value Strings
	 * @return List of T
	 */
	def query(f: ScoreDoc => T, params: Pair[String, String]*): List[T] = {
		val txt = params.map(p => p._1 + ":" + p._2).mkString(" AND ")
		logger.info("query(f, p*): params: {}", txt)
    this.query(f, txt) // searcher.search(query, 10)
	}

	def query(f: ScoreDoc => T, txt: String): List[T] = {
		val parser = new QueryParser(Version.LUCENE_34, "", analyzer)
		val query = parser.parse(txt)
    search(query, f)
	}
  
  private def search(query: Query, f: ScoreDoc => T, maxDocs: Int = 10): List[T] = {
    logger.debug("search: query: {}", query.toString)
    val topDocs = searcher.search(query, maxDocs)
  	logger.debug("search: topDocs.totalHits: {}", topDocs.totalHits)
    topDocs.scoreDocs.toList.map(f)
  }

	def exists(id: String): Boolean = {
		if (isNotEmpty) {
	    val query = new TermQuery(new Term("id", id))
	    val topDocs = searcher.search(query, 1)
			logger.info("exists: found {} entities for id: {}", topDocs.totalHits, id)
	    topDocs.totalHits > 0
		} else {
			false
		}
  }

	def all(f: ScoreDoc => T): List[T] = {
		if (isNotEmpty) {
	    val query: Query = new MatchAllDocsQuery
	    logger.debug("all: query: {}", query.toString)
	    search(query, f, size)
		} else {
			List()
		}
  }

	def init() {
		if (isEmpty) { commit() }
	}

	def size: Int = writer.numDocs()
	def isNotEmpty: Boolean = size > 0
	def isEmpty: Boolean = !isNotEmpty

	def transaction[T](f: => T) = {
		val result = f
		commit()
		result
	}

	def commit() {
		logger.info("commit")
		writer.commit()
		searcher.getIndexReader.reopen()
	}
	def rollback() {
		logger.info("rollback")
		writer.rollback() 
	}
	def optimize() { writer.optimize(true) }
	def close() {
		writer.close()
		searcher.close()
		logger.info("close: writer & searcher closed")
	}
	def clear = {
		val preClearSize= size
		writer.deleteAll()
		preClearSize
	}


	private def fileDir(name: String): File = {
		val file = new File("/tmp/core/" + name)
		if (!file.exists()) { file.mkdir() }
		file
	}
}
